# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 14:44:28 2020

@author: Stian
"""

"""
## Setup
"""

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, Activation, MaxPooling2D, Dropout, Conv2DTranspose, concatenate
from tensorflow.keras.models import Model
import argparse
import h5py
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
import time


"""
## Add save location
"""

ap = argparse.ArgumentParser()
ap.add_argument(
    "--save_folder", 
     help="path to where results is saved",
     required=True)
args = vars(ap.parse_args())
save_folder = args["save_folder"]


"""
# Setup data and rest of script
"""

print("Starter nå")

data = h5py.File('/mnt/SCRATCH/DAT300/data/train.h5', 'r')
data_test = h5py.File('/mnt/SCRATCH/DAT300/data/test.h5', 'r')
X_data = data['X'][:]
y_data = data['y'][:]
X_test = data_test['X'][:]
X_train, X_val, y_train, y_val = train_test_split(X_data, y_data, test_size=0.33, random_state=42)
del X_data, y_data, data
    
mean_vals = np.mean(X_train, axis=0)
std_val = np.std(X_train)
X_train_centered = (X_train - mean_vals)/std_val
X_val_centered = (X_val - mean_vals)/std_val
X_test_centered = (X_test - mean_vals)/std_val
y_train /= 255
y_val /= 255

print("ferdig prosessert")

"""
Version of U-Net with dropout and size preservation (padding= 'same')
""" 
def conv2d_block(input_tensor, n_filters, kernel_size = 3, batchnorm = True):
    """Function to add 2 convolutional layers with the parameters passed to it"""
    # first layer
    x = Conv2D(filters = n_filters, kernel_size = (kernel_size, kernel_size),\
              kernel_initializer = 'he_normal', padding = 'same')(input_tensor)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    # second layer
    x = Conv2D(filters = n_filters, kernel_size = (kernel_size, kernel_size),\
              kernel_initializer = 'he_normal', padding = 'same')(x)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    return x


def get_unet(input_img, n_filters = 16, dropout = 0.1, batchnorm = True, n_classes = 2):
    # Contracting Path
    c1 = conv2d_block(input_img, n_filters * 1, kernel_size = 3, batchnorm = batchnorm)
    p1 = MaxPooling2D((2, 2))(c1)
    p1 = Dropout(dropout)(p1)
    
    c2 = conv2d_block(p1, n_filters * 2, kernel_size = 3, batchnorm = batchnorm)
    p2 = MaxPooling2D((2, 2))(c2)
    p2 = Dropout(dropout)(p2)
    
    c3 = conv2d_block(p2, n_filters * 4, kernel_size = 3, batchnorm = batchnorm)
    p3 = MaxPooling2D((2, 2))(c3)
    p3 = Dropout(dropout)(p3)
    
    c4 = conv2d_block(p3, n_filters * 8, kernel_size = 3, batchnorm = batchnorm)
    p4 = MaxPooling2D((2, 2))(c4)
    p4 = Dropout(dropout)(p4)
    
    c5 = conv2d_block(p4, n_filters = n_filters * 16, kernel_size = 3, batchnorm = batchnorm)
    
    # Expansive Path
    u6 = Conv2DTranspose(n_filters * 8, (3, 3), strides = (2, 2), padding = 'same')(c5)
    u6 = concatenate([u6, c4])
    u6 = Dropout(dropout)(u6)
    c6 = conv2d_block(u6, n_filters * 8, kernel_size = 3, batchnorm = batchnorm)
    
    u7 = Conv2DTranspose(n_filters * 4, (3, 3), strides = (2, 2), padding = 'same')(c6)
    u7 = concatenate([u7, c3])
    u7 = Dropout(dropout)(u7)
    c7 = conv2d_block(u7, n_filters * 4, kernel_size = 3, batchnorm = batchnorm)
    
    u8 = Conv2DTranspose(n_filters * 2, (3, 3), strides = (2, 2), padding = 'same')(c7)
    u8 = concatenate([u8, c2])
    u8 = Dropout(dropout)(u8)
    c8 = conv2d_block(u8, n_filters * 2, kernel_size = 3, batchnorm = batchnorm)
    
    u9 = Conv2DTranspose(n_filters * 1, (3, 3), strides = (2, 2), padding = 'same')(c8)
    u9 = concatenate([u9, c1])
    u9 = Dropout(dropout)(u9)
    c9 = conv2d_block(u9, n_filters * 1, kernel_size = 3, batchnorm = batchnorm)
    
    outputs = Conv2D(n_classes, (1, 1), activation='sigmoid')(c9)
    model = Model(inputs=[input_img], outputs=[outputs])
    return model



def get_f1(y_true, y_pred): #taken from old keras source code
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    recall = true_positives / (possible_positives + K.epsilon())
    f1_val = 2*(precision*recall)/(precision+recall+K.epsilon())
    return f1_val


def make_model(n_filters, dropout, lr=0.001):
    input_img = Input(shape=(128,128,4))
    model = get_unet(input_img, n_filters = n_filters, dropout = dropout, batchnorm = True, n_classes = 1)

    model.compile(optimizer=Adam(learning_rate=lr),
                 loss='binary_crossentropy',
                 metrics=['accuracy', get_f1])
    
    return model


tic = time.time()
print(f"starting {tic}")

model = make_model(32, 0.0, 0.001)
model.summary
     
model.fit(X_train_centered,
                    y_train, batch_size=64,
                    epochs=600,
                    validation_data=(X_val_centered, y_val),
                    verbose=0,
                    callbacks= [tf.keras.callbacks.EarlyStopping(monitor='val_get_f1',
                                                                 patience=100, mode='max', restore_best_weights=True)])



# Save model
model.save(save_folder + "/best_model.h5")

# Save losses
pd.DataFrame({"loss": model.history.history["loss"],
              "val_loss": model.history.history["val_loss"],
              "f1": model.history.history["get_f1"],
              "f1_val": model.history.history["val_get_f1"]}).to_csv(save_folder + "/losses.csv", index=False)


# Last time
toc = time.time()
f = open(save_folder + "/tidbrukt.txt","w+")
f.write("tid brukt: "+ str(toc-tic ))
f.close()

# Sende inn pred
pred = model.predict(X_test_centered)
pred_flat = pred.flatten()
pred_df = pd.DataFrame(pred_flat,columns=['Predicted'])
pred_df = pred_df.reset_index()
pred_df = pred_df.rename(columns={'index':'Id'})

pp = [True if p > .5 else False for p in pred_df.Predicted]
pred_df.Predicted = pp
pred_df.to_csv(save_folder + '/pred.csv', index=False)



