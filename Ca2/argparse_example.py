# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 18:51:34 2020

@author: Runar Helin
"""

import argparse


ap = argparse.ArgumentParser()
ap.add_argument(
    "--save_folder", 
     help="path to where results is saved",
     required=True)
args = vars(ap.parse_args())
save_folder = args["save_folder"]

print(f"args: {args}")
print(f"Files will be saved to {save_folder}")
