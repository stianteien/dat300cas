# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 14:02:40 2020

@author: Stian

DAT300 CA3 Orion forsøk
"""

import argparse
import time
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split

"""
## Add save location
"""
print(tf.__version__)

ap = argparse.ArgumentParser()
ap.add_argument(
    "--save_folder", 
     help="path to where results is saved",
     required=True)
args = vars(ap.parse_args())
save_folder = args["save_folder"]

"""
## Load data
"""

tic = time.time()

y_data = pd.read_csv('/mnt/users/dat300-19/ca3/data/y_train_250.csv')
x_data_iterator = pd.read_csv('/mnt/users/dat300-19/ca3/data/X_train_250.csv', chunksize=50615)
y_data_iterator = pd.read_csv('/mnt/users/dat300-19/ca3/data/y_train_250.csv', chunksize=50615)


# Get a chunck of our data
def get_chunck(n_images=9):
    if n_images>9:
        raise KeyError('MAX 9 BILDER PER CHUCNK!!! Tar for stor plass')
    x_train = []
    y_train_batch = []
    for i, batch in enumerate(x_data_iterator):
        x_train.append(np.array(batch))

        if (i>=n_images-1):
            break
            
    for i, batch in enumerate(y_data_iterator):
        y_train_batch.append(np.array(batch))
        
        if (i>=n_images-1):
            break

    x_train = np.array(x_train) 
    y_train_batch = np.array(y_train_batch)
    
    return x_train, y_train_batch



"""
## Build model and train
"""
def make_model():
    model = tf.keras.Sequential()
    model.add(tf.keras.Input(shape=(64,)))
    
    model.add(tf.keras.layers.Dense(256, activation="relu"))
    model.add(tf.keras.layers.Dense(256, activation="relu"))
    model.add(tf.keras.layers.Dense(256, activation="relu"))
    
    
    # Make the output 1 or 0!
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    
    model.compile(optimizer='adam',
                loss='binary_crossentropy',
                metrics=['accuracy', 'Recall', 'Precision'])
    
    return model



def train_a_chunck(epochs, n_images):
    x_train, y_train_batch = get_chunck(n_images)

    #pbar = pyprind.ProgBar(len(y_train_batch))
    for i, batch in enumerate(x_train):
        
        X_train, X_val, y_train, y_val = train_test_split(batch, y_train_batch[i], test_size=0.33, random_state=42)
        
        
        # Legg til callback! 
        history = model.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=epochs, verbose=0)

        # Metrics
        precision = history.history['Precision']
        recall = history.history['Recall']
        f1 = []
        for j in range(len(recall)):
            f1.append(2 * (precision[j] * recall[j]) / (precision[j] + recall[j]))

        hist['loss'].append(history.history['loss'])
        hist['accuracy'].append(history.history['accuracy'])
        hist['f1'].append(f1)
        
        #print(history.history)

        # Metrics VALDIATON
        val_precision = history.history['val_Precision']
        val_recall = history.history['val_Recall']
        val_f1 = []
        for j in range(len(recall)):
            val_f1.append(2 * (val_precision[j] * val_recall[j]) / (val_precision[j] + val_recall[j]))

        hist_val['loss'].append(history.history['val_loss'])
        hist_val['accuracy'].append(history.history['val_accuracy'])
        hist_val['f1'].append(val_f1)

        #pbar.update()
        
def eval_a_chunck(n_images):
    x_train, y_train_batch = get_chunck(n_images)
    
    for i, batch in enumerate(x_train):
        hist_e = model.evaluate(batch, y_train_batch[i], verbose=0)
        
        #['loss', 'accuracy', 'recall', 'precision']
        # Metrics
        precision = hist_e[3]
        recall = hist_e[2]
        f1 = (2 * (precision * recall) / (precision + recall))

        hist_val['loss'].append(hist_e[0])
        hist_val['accuracy'].append(hist_e[1])
        hist_val['f1'].append(f1)
 

model = make_model()
hist = {'loss':[], 'accuracy':[], 'f1':[]}
hist_val = {'loss':[], 'accuracy':[], 'f1':[]}


# train  
runder = 50
#p1bar = pyprind.ProgBar(runder)
for z in range(runder):
    print(f"Training {z} of {runder}")
    train_a_chunck(epochs=5, n_images=1)
    #p1bar.update()



'''
# Evaluate model
runder = 50
for _ in range(runder):
    eval_a_chunck(n_images=1)
    
'''


"""
## Predict test data
"""


x_pred = []
x_test_iterator = pd.read_csv('/mnt/users/dat300-19/ca3/data/X_test_100.csv', chunksize=50615)
for i, batch in enumerate(x_test_iterator):
    pred = model.predict(batch)

    for j in pred:
        if j < .5:
            x_pred.append(False)
        else:
            x_pred.append(True)
            
            

"""
## Save data I want
"""
x_pred_df = pd.DataFrame(x_pred, columns=["Predicted"])
x_pred_df = x_pred_df.reset_index()
x_pred_df = x_pred_df.rename(columns={'index':'Id'})
x_pred_df.to_csv(save_folder + '/pred.csv', index=False)

# Save model
model.save(save_folder + "/best_model.h5")

# Save metrics
pd.DataFrame({"loss": np.array(hist["loss"]).flatten(),
              "accuracy": np.array(hist["accuracy"]).flatten(),
              "f1": np.array(hist["f1"]).flatten()}).to_csv(save_folder + "/losses.csv", index=False)

# Save val_metrics
pd.DataFrame({"loss": np.array(hist_val["loss"]).flatten(),
              "accuracy": np.array(hist_val["accuracy"]).flatten(),
              "f1": np.array(hist_val["f1"]).flatten()}).to_csv(save_folder + "/losses_val.csv", index=False)



toc = time.time()
with open(save_folder + "/tidsbrukt.txt","w+") as f:
    f.write("tid brukt: "+ str(toc-tic ))
    f.close()