#!/bin/bash
#SBATCH --ntasks=1			            # 1 core (CPU)
#SBATCH --nodes=1			            # Use 1 node
#SBATCH --job-name=orion_job	        # Name of job
#SBATCH --mem=10G 			            # Default memory per CPU is 3GB
#SBATCH --partition=smallmem                 # Use the GPU partition
#SBATCH --mail-user=steien@nmbu.no       # Your email
#SBATCH --mail-type=ALL                 # Get notifications recarding your job
#SBATCH --output=log-%j.out             # Output stored in this file


# Script commands
module purge
module load singularity

# Make a new folder in $TMPDIR
NAME="g19_orion_ca3"
SAVEFOLDER="$TMPDIR/$NAME"

mkdir $SAVEFOLDER

# RUN THE PYTHON SCRIPT
# Runs a python script named keras_mnist_example.py
# Using a singularity container named keras.sif
singularity exec --nv /mnt/SCRATCH/DAT300/container/dat300-singularity.sif python orion_ca3.py --save_folder=$SAVEFOLDER

# Copy files to location $HOME/dat300
cp -r $SAVEFOLDER $HOME/ca3

# Delete the files and folder from $TMPDIR
rm $SAVEFOLDER/*
rmdir $SAVEFOLDER
