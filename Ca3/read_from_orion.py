# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 09:14:45 2020

@author: Stian


Read the losses from orion
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

losses = pd.read_csv('losses.csv')
losses_val = pd.read_csv('losses_val.csv')

plt.plot(losses.f1)
plt.plot(losses_val.f1)
plt.show()